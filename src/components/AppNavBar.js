import {useContext} from 'react';

import {Nav,Navbar,Container} from 'react-bootstrap'

import UserContext from '../userContext';

import {Link} from 'react-router-dom';

export default function AppNavBar(){

	const {user} = useContext(UserContext);

	return (

			<Navbar bg="primary" expand="lg">
				<Container fluid>
					<Navbar.Brand to="/">E-Shop 152</Navbar.Brand>
					<Navbar.Toggle aria-controls="basic-navbar-nav" />
					<Navbar.Collapse id="basic-navbar-nav">
						<Nav className="mx-auto">
							<Link to="/" className="nav-link">Home</Link>
							<Link to="/products" className="nav-link">Products</Link>
							{
								user.id
								?
									user.isAdmin
									?
									<>
										<Link to="/addProduct" className="nav-link">Add Product</Link>
										<Link to="/logout" className="nav-link">Logout</Link>
									</>
									:
									<>
										<Link to="/cart" className="nav-link">Cart</Link>
										<Link to="/logout" className="nav-link">Logout</Link>
									</>
								:
								<>
									<Link to="/register" className="nav-link">Register</Link>
									<Link to="/login" className="nav-link">Login</Link>
								</>	
							}
						</Nav>
					</Navbar.Collapse>
				</Container>
			</Navbar>


		)

}