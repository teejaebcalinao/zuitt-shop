
import {useState} from 'react';

import {Card,Button,Col} from 'react-bootstrap';


import {Link} from 'react-router-dom'

export default function ProductCard({productProp}){


	return (
		<Col xs={12} md={3}>
			<Card className="cardHighlight">
				<Card.Header className="text-center">
					<Card.Title>
						{productProp.name}
					</Card.Title>
				</Card.Header>
				<Card.Body className="cardHighlight">
					<Card.Text>
						{productProp.description}
					</Card.Text>
					<Card.Text>
						Price: {productProp.price}
					</Card.Text>
				</Card.Body>
				<Card.Footer className="text-center">
					<Link to={`/products/viewProduct/${productProp._id}`} className="btn btn-primary">View Product</Link>
				</Card.Footer>
			</Card>
		</Col>

		)

}


/*
	Mini-activity:

	Display the description and price of our course from your courseProp.

	Take a screenshot of your page and the first CourseCard Component and send it in the hangouts.

*/