import {useState,useEffect,useContext} from 'react';

import UserContext from '../userContext';

import {Table,Button} from 'react-bootstrap';

import {Navigate} from 'react-router-dom';

export default function AdminDashboard(){

	const {user} = useContext(UserContext);

	const [allProducts,setAllProducts] = useState([]);

	function archive(productId){

		fetch(`http://localhost:4000/products/archive/${productId}`,{

			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}

		})
		.then(res => res.json())
		.then(data => {

			console.log(data);
			//refresh the page after archive
			window.location.href = "/products";

		})


	}

	useEffect(()=>{

		if(user.isAdmin){

			fetch('http://localhost:4000/products/',{

				headers: {
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				}

			})
			.then(res => res.json())
			.then(data => {

				setAllProducts(data.map(product => {

					return (

							<tr key={product._id}>
								<td>{product._id}</td>
								<td>{product.name}</td>
								<td>{product.price}</td>
								<td>{product.isActive ? "Active": "Inactive"}</td>
								<td>
									{
										product.isActive
										?
										<Button variant="danger" className="mx-2" onClick={()=>{archive(product._id)}}>Archive
										</Button>
										:
										<Button variant="success" className="mx-2">
											Activate
										</Button>
									}
								</td>
							</tr>


						)

				}))
			})

		}

	},[])

	return (
		<>
			<h1 className="my-5 text-center">Admin Dashboard</h1>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Price</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>idSample</td>
						<td>nameSample</td>
						<td>priceSample</td>
						<td>statusSample</td>
						<td>actionsSample</td>
					</tr>
						{allProducts}
				</tbody>
			</Table>
		</>
		)

}