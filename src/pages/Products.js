import {useState,useEffect,useContext} from 'react'

import {Row} from 'react-bootstrap'

import ProductCard from '../components/Product';
import AdminDashboard from '../components/AdminDashboard';

import UserContext from '../userContext';

export default function Products(){

	const {user} = useContext(UserContext);


	const [productsArray,setProductsArray] = useState([])

	useEffect(()=>{


		fetch("http://localhost:4000/products/active")
		.then(res => res.json())
		.then(data => {

			setProductsArray(data.map(product => {

				return (

						<ProductCard key={product._id} productProp={product} />

					)

			}))

		})


	},[])


	return (

		user.isAdmin
		?
		<AdminDashboard />
		:
		<>
			<h1 className="text-center my-5">E-Shop 152 Products</h1>
			<Row className="my-5">
			{productsArray}
			</Row>
		</>


	)

}
