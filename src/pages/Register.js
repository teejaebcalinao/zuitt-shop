import {useState,useContext,useEffect} from 'react';
import {Form,Button} from 'react-bootstrap';

import Swal from 'sweetalert2';

import UserContext from '../userContext';

import {Navigate} from 'react-router-dom';

export default function Register(){

	const {user} = useContext(UserContext);

	/*
		Review

		Props
			 Props is a way to pass data from a parent component to a child component. It is used like an HTML attribute added to the child component. The prop then becomes a property of the special react object that all components receive. Prop names are user-defined.

		States
			States are a way to store information within a component. This information can then be updated within the component. When a state is updated through its setter function, it will re-render the component. States are contained within their components. They are independent from other instances of the component.

		Hooks 
			Special/react-defined methods and functions that allow us to do certain tasks in our components.

		useState()
			useState() is a hook that creates states. useState() returns an array with 2 items. The first item in the array is the state and the second one is setter function. We then destructure this returned array and assign both items in variables:

			const [stateName,setStateName] = useState(initial value of State)

	*/

	/*
		Mini-Activity

		Create 6 states for the following:

		firstName
		lastName
		email
		mobileNo
		password
		confirmPassword

		Initial Values must be empty strings.

		Take a screenshot of your code and show the 6 states. Send the screenshot in the hangouts.


	*/

	//input states
	const [firstName,setFirstName] = useState("");
	const [lastName,setLastName] = useState("");
	const [email,setEmail] = useState("");
	const [mobileNo,setMobileNo] = useState("");
	const [password,setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");

	/*
		Two Way Binding

		To be able to capture/save the input value from our input elements, we can bind the value of our element with our states. We cannot type into our inputs anymore because there is now value that is bound to it. We will add an onChange event to be able to update the state that was bound to our input.

		Two Way Binding is done so that we can assure that we can save the input into our states as we type into the input element. This is so we don't have to save it just before submit.

		<Form.Control type="inputType" value={inputState} onChange={e => {setInputState(e.target.value)}}

		e = event, all event listeners can pass the event object. The event object contains all the details about the event, what happened? where did it happen? what is current value of element where the event happened?

		e.target = target is the element WHERE the event happened.

		e.target.value = value is a property of target. It is the current value of the element WHERE the event happened.


	*/


	//conditional rendering for button
	const [isActive,setIsActive] = useState(false);

	useEffect(()=>{

		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !=="" && confirmPassword !=="") && (password === confirmPassword) && (mobileNo.length === 11)){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[firstName,lastName,email,mobileNo,password,confirmPassword])



	function registerUser(e){

		//prevent submit event's default behavior
		e.preventDefault();


/*		console.log(firstName);
		console.log(lastName);
		console.log(email);
		console.log(mobileNo);
		console.log(password);
		console.log(confirmPassword);*/

		//fetch() is a JS method which allow us to pass/create a request to an api.
		//syntax: fetch("<requestURL>",{options})
		//method: the request's HTTP method.
		//headers: additional information about our request.
		//"Content-Type": "application/json" denotes that the request body contains JSON.
		//we JSON.stringify the body of our request to be able to send a JSON format string to our API.

		fetch('http://localhost:4000/users/',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({

				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password

			})
		})
		.then(res => res.json())
		.then(data => {

			//data is the response of the api/server after it's been process as JS object through our res.json() method.
			console.log(data);
			//data will only contain an email property if we can properly save our user.
			if(data.email){

				Swal.fire({

					icon:"success",
					title: "Registration Successful",
					text: "Thank you for registering!"

				})

				//redirect the user:
				window.location.href = "/login";


			} else {
				Swal.fire({

					icon: "error",
					title: "Registration Failed",
					text: "Something Went Wrong."

				})
			}

		})
	}

	return (

		user.id
		?
		<Navigate to="/courses" replace={true}/>
		:
		<>
			<h1 className="my-5 text-center">Register</h1>
			<Form onSubmit={e => registerUser(e)}>
				<Form.Group>
					<Form.Label>First Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter First Name" required value={firstName} onChange={e => {setFirstName(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Last Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter Last Name" required value={lastName} onChange={e => {setLastName(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Email:</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" required value={email} onChange={e => {setEmail(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Mobile No:</Form.Label>
					<Form.Control type="number" placeholder="Enter 11 Digit No." required value={mobileNo} onChange={e => {setMobileNo(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" required value={password} onChange={e => {setPassword(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Confirm Password:</Form.Label>
					<Form.Control type="password" placeholder="Confirm Password" required value={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}}/>
				</Form.Group>
				{
					isActive

					? <Button variant="primary" type="submit">Submit</Button>
					: <Button variant="primary" disabled>Submit</Button>
				}
			</Form>
		</>


	)


}