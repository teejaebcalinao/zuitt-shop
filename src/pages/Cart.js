import {useState,useEffect,useContext} from 'react';

import UserContext from '../userContext';

import {Card,Table,Button,InputGroup,FormControl} from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function Cart(){

	const {user} = useContext(UserContext);

	const [cart,setCart] = useState([]);
	const [cartCells,setCartCells] = useState([]);
	const [update,setUpdate] = useState(0)

	useEffect(()=>{

		let cartCheck = localStorage.getItem('cart');

		if(cartCheck){
			setCart(JSON.parse(localStorage.getItem('cart')))
		} 

	},[update])

	function deleteItem(){

	}

	function quantityHandler(value,id){

		let index = cart.findIndex(item => {

			return item.id === id

		})

		console.log(value)
		console.log(cart[index])
		console.log(JSON.parse(cart[index].quantity)+Number(value))
		setUpdate(update+1)

		cart[index].quantity=JSON.parse(cart[index].quantity)+Number(value)

		localStorage.setItem('cart',JSON.stringify(cart))

	}

	useEffect(()=>{

		if(cart.length > 0){
			setCartCells(cart.map(item=>{

				return (
						<tr key={item.id}>
							<td>{item.name}</td>
							<td>{item.price}</td>
							<td>
								<InputGroup className="mb-3">
								  <Button variant="outline-secondary" id="button-addon1" onChange={()=>quantityHandler()}>
								    -
								  </Button>
								  <FormControl
								    type="number"
								    value={JSON.parse(item.quantity)}
								    onChange={(e)=>quantityHandler(Number(e.target.value),item.id)}
								  />
								  <Button variant="outline-secondary" id="button-addon2" onChange={()=>quantityHandler()}>
								    +
								  </Button>
								</InputGroup>
							</td>
							<td>{item.subtotal}</td>
							<td>
								<Button onClick={()=>deleteItem(item.id)}>Remove</Button>
							</td>
						</tr>


					)

			}))

		}

	},[cart])




	return (



			<>
				<h1 className="text-center">Cart</h1>
				{
					cart.length > 0
					?
					<Table>
						<thead>
							<tr>
								<th>Name</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Subtotal</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							{ cartCells
							}						
						</tbody>
					</Table>
					: null

				}	
			</>


		)

}