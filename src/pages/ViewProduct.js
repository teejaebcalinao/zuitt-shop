import {useState,useEffect,useContext} from 'react';
import {Card,Button,Row,Col,InputGroup,FormControl,Badge} from 'react-bootstrap';
import {useParams,Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext'

export default function	ViewProduct(){

	const {productId} = useParams();

	const {user} = useContext(UserContext);

	const [productDetails,setProductDetails] = useState({

		name: null,
		description: null,
		price: null
	})

	const [quantity,setQuantity] = useState(1)
	const [cartQuantity,setCartQuantity] = useState(0)
	const [add,setAdd] = useState(0)

	useEffect(()=>{

		fetch(`http://localhost:4000/products/getSingleProduct/${productId}`)
		.then(res => res.json())
		.then(data => {

			setProductDetails({
				id: data._id,
				name: data.name,
				description: data.description,
				price: data.price

			})

		})


	},[productId])

	useEffect(()=>{

		let cart = JSON.parse(localStorage.getItem('cart'));
		if(cart){
			let index = cart.findIndex(item => {

				return item.id === productId

			})

			if(index >= 0){
				setCartQuantity(JSON.parse(cart[index].quantity));
			}

		}

	},[productDetails,add])


	function addToCart(){

		let cart = JSON.parse(localStorage.getItem('cart'));
		if(cart){
			let index = cart.findIndex(item => {

				return item.id === productId

			})

			if(index >= 0){
				let cartSub = productDetails.price*Number(quantity)

				cart[index].quantity=Number(cart[index].quantity)+Number(quantity);			
				cart[index].subtotal+=cartSub;

			} else {

				cart.push({

					id: productDetails.id,
					name: productDetails.name,
					price: productDetails.price,
					quantity: quantity,
					subtotal: productDetails.price*quantity

				})

			}
			
			Swal.fire({

				icon: "success",
				title: "Thank you for adding!",
				text: `You have added ${quantity} in your cart.`

			})
			setAdd(add+1)
			localStorage.setItem('cart',JSON.stringify(cart));
		} else {

			let newCart = [];
			newCart.push({

				id: productDetails.id,
				name: productDetails.name,
				price: productDetails.price,
				quantity: quantity,
				subtotal: productDetails.price*quantity

			});
			Swal.fire({

				icon: "success",
				title: "Thank you for adding!",
				text: `You have added ${quantity} in your cart.`

			})
			localStorage.setItem('cart',JSON.stringify(newCart));
		}

	}

	function addQ(){

		setQuantity(quantity+1);

	}

	function subQ(){
		if(quantity>=2){
			setQuantity(quantity-1)
		} else {
			Swal.fire({
				icon: "error",
				text: "Minimum Quantity is 1"
			})
		}
	}

	return (


			<Row className="d-flex justify-content-center align-items-center" id="single">
				<Col  xs={12} md={4}>
					<Card>
						<Card.Header className="text-center bg-warning">
							<Card.Title>
								<h3>
									{productDetails.name}								
								</h3>
							</Card.Title>
							{
								cartQuantity > 0
								?<Badge bg="secondary" className="ml-5">{cartQuantity} in cart</Badge>
								:null
							}							
						</Card.Header>
						<Card.Body className="text-center">
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{productDetails.description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{productDetails.price}</Card.Text>
							<InputGroup className="mb-3">
							  <Button variant="outline-secondary" id="button-addon1" onClick={subQ}>
							    -
							  </Button>
							  <FormControl
							    aria-label="Example text with button addon"
							    aria-describedby="basic-addon1"
							    value={quantity}
							    onChange={(e)=>setQuantity(e.target.value)}
							  />
							  <Button variant="outline-secondary" id="button-addon2" onClick={addQ}>
							    +
							  </Button>
							</InputGroup>
						</Card.Body>
						<Card.Footer className="text-center">
							{
								user.id && user.isAdmin === false
								? <Button variant="primary" className="btn-block" 
									onClick={()=>{
										addToCart()
									}}>
										Add To Cart
									</Button>
								: <Link className="btn btn-danger btn-block" to="/login">Login To Add Cart</Link>
							}
						</Card.Footer>
					</Card>
				</Col>
			</Row>

		)


}