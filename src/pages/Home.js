import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home(){


	let bannerData = {

		title: "E-Shop 152",
		description: "View our products from our catalog!",
		buttonText: "View Our Products",
		destination: "/products"

	}


	return (

		<>
			<Banner bannerProp={bannerData}/>
			<Highlights />
		</>

	)

}