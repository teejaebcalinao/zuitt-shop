
import {useState,useEffect} from 'react';

import {Container} from 'react-bootstrap';

import {BrowserRouter as Router} from 'react-router-dom';
import {Route,Routes} from 'react-router-dom'

import AppNavBar from './components/AppNavBar';

import Home from './pages/Home';
import Products from './pages/Products';
import ViewProduct from './pages/ViewProduct';
import Register from './pages/Register';
import Login from './pages/Login';
import ErrorPage from './pages/ErrorPage';
import AddProduct from './pages/AddProduct';
import Logout from './pages/Logout';
import Cart from './pages/Cart'

//import user provider from our usercontext;
import { UserProvider } from './userContext';

//import css
import './App.css';


export default function App(){


  const [user,setUser] = useState({

      id: null,
      isAdmin: null

  })


  useEffect(()=>{


    fetch('http://localhost:4000/users/getUserDetails',{

      method: 'GET',
      headers: {

        'Authorization': `Bearer ${localStorage.getItem('token')}`

      }

    })
    .then(res => res.json())
    .then(data => {

      //console.log(data);
      setUser({

        id: data._id,
        isAdmin: data.isAdmin

      })

    })

  },[])

  const unsetUser = () => {

    localStorage.clear()

  }

  console.log(user.isAdmin)
  return (
  
    <>
      <UserProvider value={{user,setUser,unsetUser}} >
        <Router>
          <AppNavBar />
          <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/login" element={<Login />} />
              <Route path="/register" element={<Register />} />
              <Route path="/addProduct" element={<AddProduct />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/cart" element={<Cart />} />
              <Route path="/products" element={<Products />} />
              <Route path="/products/ViewProduct/:productId" element={<ViewProduct />} />
              <Route path="*" element={<ErrorPage />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>

  )

}